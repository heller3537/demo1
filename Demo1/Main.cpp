// Aaron Heller
/* Demo */

#include <iostream>	
#include <conio.h>	

using namespace std;

int main() 
{
	// cout is counsel output
	cout << "Hello world!\n";

	int i;
	float f;
	double d;
	bool b;
	char c;
	const double PI = 3.1415926;

	cout << "Enter an integer:";
	cin >> i;

	if (i > 100) cout << "That's a big number.\n";
	else cout << "That's not a big number.\n";

	if (i) cout << "I is not zero.\n";

	// Switch case example online

	for (int i = 0; i < 10; i++)
	{
		cout << i << "\n";
	}

	cout << "Do you want to do math? (y/n): ";
	int x = 1;
	char input;
	cin >> input;

	while (input =='y')
	{
		cout << "9 x " << " = " << 9 * x << "\n";
		cout << "Again?";
		cin >> input;
		x++;
	}
	




	_getch();
	return 0;
}